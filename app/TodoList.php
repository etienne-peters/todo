<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    protected $table = 'todo_lists';
    public $timestamps = true;
    protected $guarded = array('id', 'created_at');
    protected $fillable = array('name', 'description');

    public function tasks(){
        return $this->belongsToMany('App\TodoTask')->withPivot('completed');;
    }

}

