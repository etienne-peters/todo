<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoListStoreRequest;
use App\Http\Requests\TodoListUpdateRequest;
use App\TodoList;
use App\TodoTask;
use Illuminate\Http\Request;

class TodoListController extends Controller
{
    public function index(TodoList $lists)
    {
        return view('lists.lists', compact('lists'));
    }

    public function create()
    {
        return view('lists.create');
    }

    public function show(TodoList $list){
        return view('lists.show', compact('list'));
    }

    public function edit(TodoList $list)
    {
        return view('lists.edit', compact('list'));
    }

    public function addTasks($listId)
    {
        $tasks = TodoTask::all()->pluck('name','id');
        $list = TodoList::findOrFail($listId);
        $listTasks = $list->tasks->pluck('name','id');

        return view('lists.addtask', compact('list','tasks','listTasks'));
    }


    public function storeTaskStatus(TodoList $list,Request $request){



        $list = TodoList::findOrFail($request->list_id);
        $target = $list->tasks->find($request->task_id);
        $list->tasks()->updateExistingPivot($target, array('completed' => ($target->pivot->completed?0:1)), false);


        //return view('lists.show', compact('list'))->with('status', 'Task '.($target->pivot->completed?'DONE':'UNDONE'));

        return redirect()->route('lists.show',$list->id)->with('status', 'Task updated');

    }

    public function storeTasks(TodoList $list, Request $request){

        $list = TodoList::findOrFail($request->id);
        $list->tasks()->detach();
        foreach($request->all() AS $key=>$value){
            $keyParts = explode('_',$key);
            if(isset($keyParts[0]) && $keyParts[0]=='task'){
                $list->tasks()->save($list,['todo_list_id' => $list->id,'todo_task_id'=> $keyParts[1]]);
            }
        }
        return redirect()->route('lists.show',$list->id)->with('status', 'Tasks added');
    }

    public function store(TodoListStoreRequest $request){
        TodoList::create($request->only('name','description'));
        return redirect()->route('lists.index')->with('status', 'List added');
    }

    public function update(TodoList $list, TodoListUpdateRequest $request){
        $list->update($request->only('name','description'));
        return redirect()->route('lists.index')->with('status', 'List Updated');
    }

    public function destroy(TodoList $list){
        $list->delete();
        return redirect()->route('lists.index')->with('status', 'List deleted');
    }
}
