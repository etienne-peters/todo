<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoTaskStoreRequest;
use App\Http\Requests\TodoTaskUpdateRequest;
use App\TodoTask;

class TodoTaskController extends Controller
{
    public function index(TodoTask $tasks)
    {
        return view('tasks.tasks', compact('tasks'));
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function show(TodoTask $task)
    {
        return view('tasks.show', compact('task'));
    }

    public function edit(TodoTask $task)
    {
        return view('tasks.edit', compact('task'));
    }


    public function store(TodoTaskStoreRequest $request){
        TodoTask::create($request->only('name','description'));
        return redirect()->route('tasks.index')->with('status', 'Tasks added'); ;
    }

    public function update(TodoTask $task, TodoTaskUpdateRequest $request){
        $task->update($request->only('name','description'));
        return redirect()->route('tasks.index')->with('status', 'Task Updated'); ;
    }

    public function destroy(TodoTask $task){
        $task->delete();
        return redirect()->route('tasks.index')->with('status', 'Task deleted'); ;
    }
}
