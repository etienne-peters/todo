<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoTask extends Model
{
    protected $table = 'todo_tasks';
    public $timestamps = true;
    protected $guarded = array('id', 'created_at');
    protected $fillable = array('name', 'description','complete');
}
