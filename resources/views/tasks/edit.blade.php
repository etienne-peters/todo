@extends('layouts.base')

@section('content')

    <h1>Edit Task</h1>
    <p class="lead">Edit your task below.</p>
    <hr>

    @include('partials.errors')
    @include('partials.status')

    {!! Form::open([
                  'method' => 'DELETE',
                  'route' => ['tasks.destroy', $task->id]
              ]) !!}
    {!! Form::submit('Delete this List?', ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}

    {!! Form::open([
        'route' => ['tasks.update', $task->id],
        'enctype'=>'multipart/data'
    ]) !!}

    @csrf
    @method('PATCH')
    {!! Form::hidden('id',$task->id) !!}
    <div class="form-group">
        {!! Form::label('name', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('name', $task->name, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', $task->description, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Save Task', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

@stop
