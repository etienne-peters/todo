@extends('layouts.base')

@section('content')

    <h1>Show Task</h1>
    <p class="lead">View your task below.</p>
    <hr>

   @include('partials.errors')
   @include('partials.status')

    <a href="{{route('tasks.edit',$task->id)}}">Edit</a>

    @csrf
    @method('PATCH')

    {!! Form::hidden('id',$task->id) !!}
    <div class="form-group">
        {!! Form::label('name', 'Title:', ['class' => 'control-label']) !!}
        <p>{{$task->name}}</p>
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
        <p>{{$task->description}}</p>
    </div>

    {!! Form::close() !!}

@stop
