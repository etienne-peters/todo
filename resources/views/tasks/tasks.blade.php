@extends('layouts.base')

@section('content')

    <h1>Todo Tasks</h1>
    <a href="{{route('tasks.create')}}"> Add new task</a>
    <hr>
    @include('partials.errors')
    @include('partials.status')
    @include('partials.taskstable')
@stop
