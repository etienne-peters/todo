@extends('layouts.base')

@section('content')

    <h1>Add a New List</h1>
    <p class="lead">Add your list below.</p>
    <hr>

   @include('partials.errors')
   @include('partials.status')

    {!! Form::open([
        'route' => 'lists.store',
        'enctype'=>'multipart/data'
    ]) !!}

    @csrf
    <div class="form-group">
        {!! Form::label('name', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Create New List', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

@stop
