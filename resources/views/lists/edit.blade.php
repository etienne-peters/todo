@extends('layouts.base')

@section('content')
    <h1>Edit List</h1>
    <p class="lead">Edit your list below.</p>
    <hr>

   @include('partials.errors')
   @include('partials.status')

    {!! Form::open([
                  'method' => 'DELETE',
                  'route' => ['lists.destroy', $list->id]
              ]) !!}
    {!! Form::submit('Delete this List?', ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}

    {!! Form::open([
        'route' => ['lists.update', $list->id],
        'enctype'=>'multipart/data'
    ]) !!}

    @csrf
    @method('PATCH')

    {!! Form::hidden('id',$list->id) !!}
    <div class="form-group">
        {!! Form::label('name', 'Title:', ['class' => 'control-label']) !!}
        {!! Form::text('name', $list->name, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', $list->description, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Save List', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}

@stop
