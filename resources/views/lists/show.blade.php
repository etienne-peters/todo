@extends('layouts.base')

@section('content')

    <h1>Show List</h1>
    <p class="lead">View your list below.</p>
    <hr>

    @include('partials.errors')
    @include('partials.status')

    <a href="{{route('lists.edit',$list->id)}}">Edit</a>
    <a href="{{route('lists.addtasks',$list->id)}}">Manage Tasks</a>

    @csrf
    @method('PATCH')

    {!! Form::hidden('id',$list->id) !!}
    <div class="form-group">

        {!! Form::label('name', 'Title:', ['class' => 'control-label']) !!}
        <p>{{$list->name}}</p>
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
        <p>{{$list->description}}</p>
    </div>

    @if($list->tasks->count()>0)
        <h2>Tasks</h2>
        <table>
            <thead>
                <th>Name</th>
                <th>Description</th>
                <th>Status</th>
                <th>Action</th>

            </thead>
            <tbody>
        @foreach($list->tasks AS $task)

            <tr>
                <td>{{$task->name}}</td>
                <td>{{$task->description}}</td>
                <td>{{($task->pivot->completed?'Completed':'Open')}}</td>
                <td>{!! Form::open([
                  'method' => 'POST',
                  'route' => ['lists.storetasksstatus', $task->id]
              ]) !!}
                    {!! Form::hidden('list_id',$list->id) !!}
                    {!! Form::hidden('task_id',$task->id) !!}
                    {!! Form::submit(($task->pivot->completed?'Uncomplete':'Complete'), ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}</td>

            </tr>


        @endforeach
            </tbody>
        </table>
    @endif

    {!! Form::close() !!}

@stop
