@extends('layouts.base')

@section('content')
   <h1>Todo Lists</h1>
   <a href="{{route('lists.create')}}"> Add new list</a>
   <hr>

   @include('partials.errors')
   @include('partials.status')
   @include('partials.liststable')
@stop
