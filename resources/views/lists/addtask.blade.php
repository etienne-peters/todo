@extends('layouts.base')

@section('content')

    <h1>Manage Tasks</h1>
    <hr>

    @include('partials.errors')
    @include('partials.status')

    {!! Form::open([
        'route' => ['lists.storetasks', $list->id],
        'enctype'=>'multipart/data'
    ]) !!}

    @csrf
    {!! Form::hidden('id',$list->id) !!}
    <div class="form-group">
        @foreach($tasks AS $taskId => $taskName)
            {!! Form::label('name', $taskName, ['class' => 'control-label']) !!}
            {!! Form::checkbox('task_'.$taskId, $taskName, in_array($taskId,array_keys($listTasks->toArray()))); !!}
            <br>
        @endforeach
    </div>
    {!! Form::submit('Save Tasks', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop
