<table>
    <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th></th>
    </thead>
    <tbody>
        @foreach($tasks->all() AS $task)
            <tr>
                <td>{{$task->id}}</td>
                <td>{{$task->name}}</td>
                <td>{{$task->description}}</td>
                <td><a href="{{route('tasks.show',$task->id)}}">Details</a></td>
            </tr>
        @endforeach
    </tbody>
</table>
