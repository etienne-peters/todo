<table>
    <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th></th>
    </thead>
    <tbody>
        @foreach($lists->all() AS $list)
            <tr>
                <td>{{$list->id}}</td>
                <td>{{$list->name}}</td>
                <td>{{$list->description}}</td>
                <td><a href="{{route('lists.show',$list->id)}}">Details</a></td>
            </tr>
        @endforeach
    </tbody>
</table>
